// 引入样式
import './style/index.less';
// 引入游戏控制类
import GameControl from './modules/GameControl';
// 开启游戏
const gameControl = new GameControl();

gameControl.init();