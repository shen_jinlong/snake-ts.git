// 引入其他的类
import Snake from "./Snake";
import Food from "./Food";
import ScorePanel from "./ScorePanel";

/** 游戏控制器，控制其他所有类 */
class GameControl {
    // 定义三个属性
    snake: Snake;               // 蛇
    food: Food;                 // 食物
    scorelPanel: ScorePanel;    // 记分牌
    direction: string = 'ArrowRight';     // 蛇的移动方向
    isLive: boolean = true; // 记录游戏是否结束

    constructor() {
        this.snake = new Snake();
        this.food = new Food();
        this.scorelPanel = new ScorePanel();
    }

    /** 游戏的初始化方法 */
    init() {
        // 绑定键盘按下的事件
        // document.addEventListener('keydown', this.keydownHandler.bind(this));
        // document.addEventListener('keydown', (event) => this.keydownHandler(event));
        document.addEventListener('keydown', this.keydownHandler);
        // 调用蛇移动
        this.run();
    }

    /** 创建一个键盘按下的响应函数 */
    keydownHandler = (event: KeyboardEvent) => {
        this.direction = event.key;
    }
    // keydownHandler(event: KeyboardEvent) {
    //     this.direction = event.key;
    // }

    /** 创建一个控制蛇移动的方法 */
    run() {
        /**
         * 根据方向（this.direction）来使蛇的位置改变
         * 向上——top 减少
         * 向下——top 增加
         * 向左——left 减少
         * 向右——left 增加
         */
        // 获取蛇的坐标
        let X = this.snake.X;
        let Y = this.snake.Y;
        switch (this.direction) {
            case "ArrowUp":
                // 向上移动top减少
                Y -= 10;
                break;
            case "ArrowDown":
                // 向下移动top增加
                Y += 10;
                break;
            case "ArrowLeft":
                // 向左移动left减少
                X -= 10;
                break;
            case "ArrowRight":
                // 向右移动left增加
                X += 10;
                break;

        }

        // 吃到食物
        this.checkEat(this.snake.X, this.snake.Y);

        // 修改蛇的位置
        try {
            this.snake.X = X;
            this.snake.Y = Y;
        } catch (e: any) {
            // 进入到catch，出现异常，游戏结束，弹出一个提示
            alert(e.message);
            this.isLive = false;
        }
        // 开启定时调用
        this.isLive && setTimeout(this.run.bind(this), 300 - (this.scorelPanel.level - 1) * 30);
        // setTimeout(() => {
        //     this.run();
        // }, 300 - (this.scorelPanel.level - 1) * 30);

    }

    // 检测蛇是否吃到食物
    checkEat(X: number, Y: number) {
        if (X === this.food.X && Y === this.food.Y) {
            // 更新食物位置
            this.food.change();
            // 分数增加
            this.scorelPanel.addScore();
            // 增长蛇
            this.snake.addBody();
        }
    }

}

export default GameControl;