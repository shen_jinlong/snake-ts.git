class Snake {
    head: HTMLElement;
    // 蛇的身体（包括头部）
    bodies: HTMLCollection;
    // 获取蛇的容器
    element: HTMLElement;

    constructor() {
        this.element = document.getElementById('snake')!;
        this.head = document.querySelector('#snake > div')!;
        this.bodies = this.element.getElementsByTagName('div');
    }

    /** 获取蛇头坐标 */
    get X() {
        return this.head.offsetLeft;
    }

    /** 获取蛇的轴坐标 */
    get Y() {
        return this.head.offsetTop;
    }

    /** 设置蛇头的坐标 */
    set X(value: number) {
        if (this.X === value) {
            return;
        }

        // 撞墙检测
        if (value < 0 || value > 290) {
            // 撞墙啦
            throw new Error('游戏结束！')
        }

        // 设置不能反方向移动
        if (this.bodies[1] && (this.bodies[1] as HTMLElement).offsetLeft === value) {
            value += 10;
            if (value > this.X) {
                value = this.X - 10;
            } else {
                value = this.X + 10;
            }
        }

        // 蛇移动
        this.moveBody();
        this.head.style.left = value + 'px';
        this.checkHeadBody();
    }

    set Y(value: number) {
        if (this.Y === value) {
            return;
        }
        // 撞墙检测
        if (value < 0 || value > 290) {
            // 撞墙啦
            throw new Error('游戏结束！')
        }

        // 设置不能反方向移动
        if (this.bodies[1] && (this.bodies[1] as HTMLElement).offsetTop === value) {
            value += 10;
            if (value > this.Y) {
                value = this.Y - 10;
            } else {
                value = this.Y + 10;
            }
        }

        // 蛇移动
        this.moveBody();
        this.head.style.top = value + 'px';
        this.checkHeadBody();
    }

    /** 蛇增加身体 */
    addBody() {
        this.element.insertAdjacentHTML("beforeend", "<div></div>");
    }

    /** 蛇的移动 */
    moveBody() {
        /**
         * 后一节的身体等于前一节的身体位置
         * eg:
         * 第四节 = 第三节位置
         * 第三节 = 第二节位置
         * 第二节 = 第一节位置
         * 第一节自己走了
         */
        for (let i = this.bodies.length - 1; i > 0; i--) {

            // 获取前一节身体位置
            let X = (this.bodies[i - 1] as HTMLElement).offsetLeft;
            let Y = (this.bodies[i - 1] as HTMLElement).offsetTop;

            // 设置为当前身体
            (this.bodies[i] as HTMLElement).style.left = X + 'px';
            (this.bodies[i] as HTMLElement).style.top = Y + 'px';
        }
    }

    /** 判断蛇头是否撞了身体 */
    checkHeadBody() {
        // 获取所有身体，检查是否和蛇头的坐标发生重叠
        for (let i = 1; i < this.bodies.length; i++) {
            let bd = this.bodies[i] as HTMLElement;
            if (this.X === bd.offsetLeft && this.Y === bd.offsetTop) {
                throw new Error('游戏结束！')
            }
        }
    }
}

export default Snake;