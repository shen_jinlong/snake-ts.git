/** 定义食物类 */
class Food {

    /* 属性 */
    element: HTMLElement;   // 食物元素实例

    constructor() {
        // 获取dom元素
        this.element = document.getElementById('food')!;
    }

    /* 方法 */
    /** 定义一个获取食物X轴坐标的方法 */
    get X() {
        return this.element.offsetLeft;
    }
    /** 定义一个获取食物Y轴坐标的方法 */
    get Y() {
        return this.element.offsetTop;
    }

    /** 生成随机位置 */
    change() {
        /**
         * 生成随机位置
         * 1. 取值范围（0～290）&& 10的倍数（配合蛇的运动）
         */
        let elementLeft = Math.round(Math.random() * 29) * 10;
        let elementTop = Math.round(Math.random() * 29) * 10;
        // Math.floor(Math.random() * 30) * 10; // 向下取整
        this.element.style.left = elementLeft + 'px';
        this.element.style.top = elementTop + 'px';
    }
}

export default Food;