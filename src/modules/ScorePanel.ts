/** 记分牌类 */
class ScorePanel {

    /* 属性 */
    score = 0;
    level = 1;
    scoreElement: HTMLElement;
    levelElement: HTMLElement;
    maxLevel: number; // 最大等级
    upScore: number; // 升级需要的分数

    constructor(maxLevel: number = 10, upScore: number = 10) {
        this.scoreElement = document.getElementById('score')!;
        this.levelElement = document.getElementById('level')!;
        this.maxLevel = maxLevel;
        this.upScore = upScore;
    }

    /** 方法 */
    /** 得分 */
    addScore() {
        this.scoreElement.innerHTML = ++this.score + '';
        if (this.score % this.upScore === 0) {
            this.upLevel();
            // this.upScore = this.upScore * 2;
        }
    }

    /** 升级 */
    upLevel() {
        if (this.level < this.maxLevel) {
            this.levelElement.innerHTML = ++this.level + '';
        }
    }

}

export default ScorePanel;